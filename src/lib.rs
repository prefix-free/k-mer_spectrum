use bio::data_structures::suffix_array::suffix_array;
use bio::data_structures::suffix_array::lcp as lcp_array;

// num\[k\] is the number of distinct k+1-mers in the string \
// runs in linear time \
// lcp gives us information where distinct kmers start \
// e.g. lcp\[i\] = 1 => 1mer is not new distinct, 2mer is \
// highlight wheeler matrix from lcp\[i\] to $, then sum highlighted in columns
// fn kmer_spectrum(sa: &Vec<usize>, lcp: &Vec<isize>) -> Vec<usize> {}

/// longest remaining suffix 
fn lrs_array(sa: &[usize], len: &[usize]) -> Vec<usize> {
    let n = sa.len();
    let mut isa = vec![0; n];
    for i in 0..n { isa[sa[i]] = i; }

    let mut result = vec![0; n];
    let mut lengths = len.iter().rev();
    let mut suffix_len = 0;
    let mut phrase_len = *lengths.next().unwrap();
    for i in 0..n {
        result[isa[n-1-i]] = suffix_len;

        suffix_len += 1;
        if suffix_len > phrase_len { 
            suffix_len = 0;
            phrase_len = match lengths.next() {
                Some(x) => { *x },
                None    => {  n }
            }
        }
    }
    return result;
}

pub fn union_kmers(concat: &[u8], len: &[usize]) -> Vec<usize> {
    let max = *len.iter().max().unwrap();
    let sa = suffix_array(concat);
    let mut lcp = lcp_array(concat, &sa).decompress();
    lcp[0] = 0;
    let dollars = lrs_array(&sa, len);

    let mut starts = vec![0; max+1];
    let mut ends = vec![0; max+1];
    for i in 0..sa.len() {
        if lcp[i] as usize >= dollars[i] { continue; }
        starts[lcp[i] as usize] += 1;
        ends[dollars[i]] += 1;
    }
    let mut kmers = vec![starts[0] - ends[0]; max+1];
    for i in 1..max+1 { kmers[i] = kmers[i-1] + starts[i] - ends[i]; }
    return kmers;
}

pub fn preprocess_input(input: &[&[u8]]) -> (Vec<u8>, Vec<usize>) {
    let mut len = Vec::new();
    for i in 0..input.len() {
        len.push(input[i].len());
    }

    let mut seq = Vec::new();
    for i in 0..input.len() {
        seq.extend_from_slice(input[i]);
        seq.push(b'$');
    }

    return (seq, len);
}


#[cfg(test)]
mod tests {
    use super::*;
    use proptest::test_runner::TestRunner;
    use proptest::strategy::{Strategy, ValueTree};

    #[test]
    fn test_union_kmers() {
        let mut s = Vec::from("ACAAACATAT");
        let l = vec![s.len()];
        s.push(b'$');

        let spectrum = union_kmers(&s, &l);
        assert_eq!(vec![3, 5, 7, 7, 6, 5, 4, 3, 2, 1, 0], spectrum);
    }

    #[test]
    fn test_union_on_more_sequences() {
        let input: Vec<&[u8]> = vec![b"ACAAACATAT", b"ACATA"];
        let (s, l) = preprocess_input(&input);

        let spectrum = union_kmers(&s, &l);
        assert_eq!(vec![3, 5, 7, 7, 6, 5, 4, 3, 2, 1, 0], spectrum);
    }

    #[test]
    fn test_union_on_suffixes() {
        let mut s = vec![b"ACAAACATAT".to_vec(), b"ACATAT".to_vec()];
        let mut l = Vec::new();

        for i in 0..s.len() {
            l.push(s[i].len());
            s[i].push(b'$');
        }
        let s = s.concat();
        let spectrum = union_kmers(&s, &l);
        let expected = vec![3, 5, 7, 7, 6, 5, 4, 3, 2, 1, 0];
        assert_eq!(expected, spectrum);
    }

    #[test]
    fn test_spectrum() {
        let input: Vec<&[u8]> = vec![b"ACAAACATAT"];
        let (s, l) = preprocess_input(&input);

        let spectrum = union_kmers(&s, &l);
        assert_eq!(vec![3, 5, 7, 7, 6, 5, 4, 3, 2, 1, 0], spectrum);
    }

    #[test]
    fn test_simple_string() {
        let input: Vec<&[u8]> = vec![b"AAAAAAAAAAAAAAAAA"];
        let (s, l) = preprocess_input(&input);

        let spectrum = union_kmers(&s, &l);

        // assert_eq!(vec![1; input[0].len()], spectrum);
        let mut expected_result = vec![1; input[0].len()];
        expected_result.push(0);
        assert_eq!(expected_result, spectrum);
    }

    #[test]
    fn test_de_bruijn_string() {
        let input: Vec<&[u8]> = vec![b"AACCAGCGGATCTGTTA"];  // deBruijn sequence (k=2, |sigma|=4)
        let (s, l) = preprocess_input(&input);

        let spectrum = union_kmers(&s, &l);

        assert_eq!(vec![4, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0], spectrum);
    }

    #[test]
    fn test_same_sequences() {
        let input: Vec<&[u8]> = vec![
            b"ACGTGCA",
            b"ACGTGCA"
        ];
        let (s, l) = preprocess_input(&input);

        let spectrum = union_kmers(&s, &l);
        let expected = vec![4, 6, 5, 4, 3, 2, 1, 0];
        assert_eq!(spectrum, expected);
    }

    #[test]
    fn test_almost_same() {
        let input: Vec<&[u8]> = vec![
            b"ACGTGATCGACGT",
            b"ACGTGACCGACGT"
        ];
        let (s, l) = preprocess_input(&input);
        let spectrum = union_kmers(&s, &l);
        let expected = vec![4, 8, 11, 13, 14, 14, 14, 12, 10, 8, 6, 4, 2, 0];
        assert_eq!(spectrum, expected);
        println!("{:?}", spectrum);
    }

    #[test]
    fn test_proptest() {
        let mut runner = TestRunner::default();
        let seq = "[ACGT]{1,10}".new_tree(&mut runner).unwrap();
        println!("{}", seq.current());
    }
}


